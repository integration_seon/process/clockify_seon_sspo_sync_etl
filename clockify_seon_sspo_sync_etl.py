from pprint import pprint
import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
import logging
from clockify_seon_sspo_client.factory.factory import Factory
from clockify_seon_sspo_client.factory.service_enum import Service

logging.basicConfig(level=logging.INFO)

args = {
    'owner': 'seon',
    'start_date': airflow.utils.dates.days_ago(1)
}

dag = DAG (
    dag_id= 'clockify_seon_sspo_sync_etl',
    default_args=args,
    schedule_interval = None,
)


def __retrive_config(kwargs):
    conf = kwargs['dag_run'].conf
    organization_uuid = conf['organization_uuid']
    application_uuid = conf['application_uuid']
    secret = conf['secret']
    
    return organization_uuid, application_uuid, secret

def retrive_scrum_complex_projects(**kwargs):
    try:

        pprint ("retrive scrum complex project")
        
        organization, application, clockify_key = __retrive_config(kwargs)
        
        service = Factory.create(Service.ComplexSoftwareProject)
        service.integrate(organization,clockify_key,application)

    except Exception as e: 
        logging.error("OS error: {0}".format(e))
        logging.error(e.__dict__)
    
def retrive_scrum_atomic_projects(**kwargs):
    pprint ("retrive scrum atomic project")
    
    organization, application, clockify_key = __retrive_config(kwargs)
        
    service = Factory.create(Service.AtomicSoftwareProject)
    service.integrate(organization,clockify_key,application)


def retrive_team_members(**kwargs):
    pprint ("retrive scrum team")
    
    organization, application, clockify_key = __retrive_config(kwargs)
        
    service = Factory.create(Service.TeamMember)
    service.integrate(organization,clockify_key,application)


def retrive_scrum_development_tasks(**kwargs):
    pprint ("retrive scrum development task")
    
    organization, application, clockify_key = __retrive_config(kwargs)
        
    service = Factory.create(Service.ScrumPerformedTask)
    service.integrate(organization,clockify_key,application)

    
def retrive_stakeholder_participation(**kwargs):
    pprint ("retrive stakeholder participation ")

    organization, application, clockify_key = __retrive_config(kwargs)
        
    service = Factory.create(Service.StakeholderParticipation)
    service.integrate(organization,clockify_key,application)

## Operators
 
retrive_scrum_complex_projects_operator = PythonOperator(
    task_id = 'retrive_scrum_complex_projects',
    provide_context=True,
    python_callable=retrive_scrum_complex_projects, 
    dag=dag,
)

retrive_scrum_atomic_projects_operator = PythonOperator(
    task_id = 'retrive_scrum_atomic_projects',
    provide_context=True,
    python_callable=retrive_scrum_atomic_projects, 
    dag=dag,
)

retrive_team_members_operator = PythonOperator(
    task_id = 'retrive_team_members',
    provide_context=True,
    python_callable=retrive_team_members, 
    dag=dag,
)

retrive_scrum_development_tasks_operator = PythonOperator(
    task_id = 'retrive_scrum_development_tasks',
    provide_context=True,
    python_callable=retrive_scrum_development_tasks, 
    dag=dag,
)

retrive_stakeholder_participation_operator = PythonOperator(
    task_id = 'retrive_stakeholder_participation',
    provide_context=True,
    python_callable=retrive_stakeholder_participation, 
    dag=dag,
)

## Workflow
retrive_scrum_complex_projects_operator >> retrive_scrum_atomic_projects_operator >> retrive_team_members_operator >> retrive_scrum_development_tasks_operator >> retrive_stakeholder_participation_operator
